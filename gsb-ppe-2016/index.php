<?php
/**
 * FICHIER INDEX.PHP
 * Point d'entrée dans l'application
 * Créé par : Semih
 * Le : 23/11/2016
 */

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On'); 
session_start();
include ('modeles/class.pdoGSB.inc.php');

	$pdo = PdoGSB::getPdoGSB();

        // On verifie s'il la variable globale session admin existe
	if(isset($_SESSION["admin"]))
        {
            include ('vues/v_enteteAdmin.php');
            include ('vues/v_menuAdmin.php');
        }	
	else
        {
            include ('vues/v_entete.php');
            include ('vues/v_menu.php');
	}
	
	

        // On verifie s'il existe bien une valeur à la variable uc récupérer dans l'URL
	if(!isset($_REQUEST['uc']))
	{
		$uc = 'accueil';
	}
	else
	{
            $uc = $_REQUEST['uc'];
	} 
	

	//Affichage d'un message d'erreur si nécessaire.
	/*if(isset($erreur))
        {
		include ('vues/v_erreur.php');
	}*/

	switch ($uc)
	{
		case "accueil":
			include ('vues/v_corps.php');
			break;
                    
                case "connexion":
			include ('controleurs/c_connexion.php');
			break;
                    
                case "admin" :
                {
                    if(isset($_SESSION['admin']))
                    {
                        include ('controleurs/c_gestionAdmin.php');
                        break;
                    }
                    else
                    {
                        echo "<script>document.location.replace('index.php');</script>";                         
                    }
                }
	}

	include ('vues/v_footer.php');
?>