<?php

/**
 * FICHIER INDEX.PHP
 * Point d'entrée dans l'application
 * Créé par : Semih
 * Le : 23/11/2016
 */


class PdoGSB
{   		
      	private static $serveur='mysql:host=172.17.0.17';
      	private static $bdd='dbname=gsb_ppe2016';
      	private static $user='semih' ;    		
      	private static $mdp='btssio' ;	
		private static $monPdo;
		private static $monPdoGSB = null;
/**
 * Constructeur privé, crée l'instance de PDO qui sera sollicitée
 * pour toutes les méthodes de la classe
 */				
	private function __construct()
	{
		PdoGSB::$monPdo = new PDO(PdoGSB::$serveur.';'.PdoGSB::$bdd, PdoGSB::$user, PdoGSB::$mdp);
		PdoGSB::$monPdo->query("SET CHARACTER SET utf8");
	}
	public function _destruct(){
		PdoGSB::$monPdo = null;
	}
/**
 * Fonction statique qui crée l'unique instance de la classe
 *
 * Appel : PdoGSB = PdoGSB::getPdoGSB();
 * @return l'unique objet de la classe PdoGSB
 */
	public static function getPdoGSB()
	{
		if(PdoGSB::$monPdoGSB == null)
		{
			PdoGSB::$monPdoGSB= new PdoGSB();
		}
		return PdoGSB::$monPdoGSB;
	}


	public function connexionAdmin($login, $mdp)
        {
		$req = "select count(*) as nb from Admin where login='".$login."' and mdp='".$mdp."'";
		$res = PdoGSB::$monPdo->query($req);
		$LaLigne = $res->fetch();
		$nombre = $LaLigne['nb'];
		if ($nombre == 1)
                {
                    $_SESSION["login"] = $_POST["login"];
                    return true;
		}
                else
                {
                    return false;
		}
	}
        
        public function connexionEmploye($login, $mdp)
        {
		$req = "select count(*) as nb from employes where login='".$login."' and mdp='".$mdp."'";
		$res = PdoGSB::$monPdo->query($req);
		$LaLigne = $res->fetch();
		$nombre = $LaLigne['nb'];
		if ($nombre == 1)
                {
                    $_SESSION["login"] = $_POST["login"];
                    return true;
		}
                else
                {
                    return false;
		}
	}
}
?>